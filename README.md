# README

This README would normally document whatever steps are necessary to get your application up and running.

### Objetivo del repositorio

- Minisite para comunicar sobre el impacto del Coronavrus COVID-19 en España

### How do I get set up?

La storytellingrmación en bruto está recogida en el directorio de Drive [SQY-COVID-19](https://drive.google.com/open?id=1PXeeeoEVxrQGglPTGGLKmLLK-BO4g5Sw)

#### Configuracion

WIP - enrique.sanz@secuoyas.com

### Despliegue - Instrucciones 

- Para testing: Hay que mergear en la rama `release/test` y se desplegará en https://pre.coronavirus.secuoyas.com/.  
- Para producción: Hay que mergear en la rama `release/production` y entrar en pipelines y ejecutar manualmente el deploy. Se desplegará en https://coronavirus.secuoyas.com/  

#### Dependencias

- COVID-19-Maps - Mapa ES en D3 - dev por maria.badanina@secuoyas.com
- COVID-API - API creada en Swagger para servir los datos por chema.diezdelcorral@secuoyas.com



### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Contactos

- daniel.serrano@secuoyas.com
- rubi@secuoyas.com
- gema.hoang@secuoyas.com
- chema.diezdelcorral@secuoyas.com
- maria.badanina@secuoyas.com
