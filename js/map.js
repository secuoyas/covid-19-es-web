"use strict";

//Define map projection
const projection = d3.geoMercator().center([-4, 38]).scale(1600);

// Generate map path
const path = d3.geoPath().projection(projection);

// Create SVG container for map
const mapSvgContainer = d3
  .select("#map-container")
  .append("svg")
  .attr("viewBox", `330 20 370 370`)
  .append("g");

// Create container for charts
const chartSvgContainer = d3
  .select("#chart-container")
  .append("svg")
  .attr("viewBox", `10 -120 840 750`);

// Create SVG contaienr for legend
const legendSvgContainer = d3
  .select("#legend-container")
  .append("svg")
  .attr("viewBox", `0 0 140 500`)
  .attr("class", "map-legend");

// Defind a gradient for legend for confirmed cases
const legend = legendSvgContainer
  .append("defs")
  .append("svg:linearGradient")
  .attr("id", "gradient")
  .attr("x1", "100%")
  .attr("y1", "0%")
  .attr("x2", "100%")
  .attr("y2", "100%")
  .attr("spreadMethod", "pad");

legend
  .append("stop")
  .attr("offset", "0%")
  .attr("stop-color", "#FA0067")
  .attr("stop-opacity", 1);

legend
  .append("stop")
  .attr("offset", "90%")
  .attr("stop-color", "#fff")
  .attr("stop-opacity", 1);

legend
  .append("stop")
  .attr("offset", "100%")
  .attr("stop-color", "#fff")
  .attr("stop-opacity", 1);

// Defind a gradient for legend per cápita
const legendPerCapita = legendSvgContainer
  .append("defs")
  .append("svg:linearGradient")
  .attr("id", "gradient-percapita")
  .attr("x1", "100%")
  .attr("y1", "0%")
  .attr("x2", "100%")
  .attr("y2", "100%")
  .attr("spreadMethod", "pad");

legendPerCapita
  .append("stop")
  .attr("offset", "0%")
  .attr("stop-color", "#FFAA25")
  .attr("stop-opacity", 1);

legendPerCapita
  .append("stop")
  .attr("offset", "90%")
  .attr("stop-color", "#fff")
  .attr("stop-opacity", 1);

legendPerCapita
  .append("stop")
  .attr("offset", "100%")
  .attr("stop-color", "#fff")
  .attr("stop-opacity", 1);

legendSvgContainer
  .append("rect")
  .attr("width", 27)
  .attr("height", 300)
  .attr("stroke", "lightGray")
  .style("fill", "url(#gradient)")
  .attr("transform", "translate(0,10)");

// Make a call to api and local data files
Promise.all([
  d3.json("data/spain/map.json"),
  d3.json("data/spain/populatonCCAA.json"),
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?ultimodia=true"),
])
  .then((data) => {
    // create map array
    this.featureCollection = data[0].features;

    // create population array
    const populationCollecton = data[1];

    // create array of confirmet cases
    const CasosCollection = data[2];

    // Extracti timeline
    const communitiesArr = CasosCollection.timeline[0].regiones;
    const dataArr = communitiesArr.map((item) => {
      return item.data;
    });

    // Extract population data per comunidad
    const populationCCAA = populationCollecton.map((comunidadPopulation) => {
      return comunidadPopulation;
    });

    // Add population file to map file
    featureCollection.map((d, i) => {
      Object.assign(d.properties, populationCCAA[i]);
    });

    // Add Data from endpoint to map file
    featureCollection.map((d, i) => {
      Object.assign(d.properties, dataArr[i]);
    });

    // Add Name from endpoint to map file
    featureCollection.map((d, i) => {
      Object.assign(d.properties, communitiesArr[i]);
    });

    // console.log("afret third assign:", featureCollection);

    // Casos per cápita
    this.perCapita = featureCollection.map((item, idx) => {
      return {
        ["name"]: item.properties.nombreLugar,
        ["casosPerCapita"]:
          (item.properties.casosConfirmados / item.properties.population) *
          100000,
      };
    });

    // Add casos per cápita
    featureCollection.map((d, i) => {
      Object.assign(d.properties, perCapita[i]);
    });

    //-------------------------Bar chart----------------------------//

    this.sortByCases = () => {
      featureCollection.sort((a, b) => {
        return d3.ascending(
          parseInt(a.properties.casosConfirmados),
          parseInt(b.properties.casosConfirmados)
        );
      });
    };
    sortByCases();

    this.sortByCapita = () => {
      perCapita.sort((a, b) => {
        return d3.ascending(
          parseInt(a.casosPerCapita),
          parseInt(b.casosPerCapita)
        );
      });
    };
    sortByCapita();

    // --------------- Axises for charts ----------------//

    // X axis
    const x = d3
      .scaleBand()
      .domain(
        featureCollection.map((d) => {
          return d.properties.nombreLugar;
        })
      )
      .range([0, 550])
      .paddingInner(10);

    // Y axis for infected data for confirmed cases
    this.yInfected = d3
      .scaleLinear()
      .domain([
        0,
        Math.ceil(
          d3.max(featureCollection, (d) => {
            return parseInt(d.properties.casosConfirmados);
          }) / 10000
        ) * 10000,
      ])
      .range([0, 300]);

    // Y axis for infected data for cases per cápita
    this.yInfectedPerCapita = d3
      .scaleLinear()
      .domain([
        0,
        Math.ceil(
          d3.max(perCapita, (d) => {
            return parseInt(d.casosPerCapita);
          }) / 1000
        ) * 1000,
      ])
      .range([0, 300]);

    const rects = chartSvgContainer.selectAll("rect").data(featureCollection);

    // Name of communities
    const xAxisCall = d3.axisTop(x);
    chartSvgContainer
      .append("g")
      .attr("class", "x-axis-barchart")
      .attr("transform", "translate(110, 220)")
      .call(xAxisCall)
      .selectAll("text")
      .attr("class", "chart-label")
      .attr("font-size", "14")
      .attr("y", "4")
      .attr("x", "-15")
      .attr("text-anchor", "end")
      .attr("transform", "rotate(90)");

    // Axis call for confirmed cases
    this.yAxisCall = d3.axisRight(yInfected).ticks(5).tickSizeOuter(-580);

    //  Axis call for cases per cápita
    this.yAxisCallPerCapita = d3
      .axisRight(yInfectedPerCapita)
      .ticks(5)
      .tickSizeOuter(-580);

    chartSvgContainer
      .append("g")
      .attr("class", "y-axis-barchart")
      .attr("transform", "translate(680, 220)")
      .call(yAxisCall)
      .selectAll("text")
      .attr("y", "-10")
      .attr("x", "15")
      .attr("text-anchor", "start")
      .attr("transform", "rotate(90)translate(-20, -3)");

    chartSvgContainer
      .append("text")
      .attr("class", "chart-name")
      .text("Casos confirmados")
      .attr("transform", "rotate(90)translate(220, -735)");

    // // create grid
    chartSvgContainer
      .selectAll(".x-axis-barchart")
      .selectAll("line")
      .attr("y2", 300)
      .attr("stroke", "#EBEBEB")
      .attr("transform", "translate(-15)");

    chartSvgContainer
      .selectAll(".y-axis-barchart")
      .selectAll("line")
      .attr("y2", 580)
      .attr("stroke", "#EBEBEB")
      .attr("transform", "rotate(90.6)translate(0, 5)");

    // // Hide domain line in x axis
    chartSvgContainer
      .selectAll(".x-axis-barchart")
      .selectAll(".domain")
      .attr("visibility", "hidden");

    // // Hide domain line in x axis
    chartSvgContainer
      .selectAll(".y-axis-barchart")
      .selectAll(".domain")
      .attr("transform", "translate(-5)");

    chartSvgContainer
      .selectAll(".y-axis-barchart")
      .selectAll(".domain")
      // .attr("visibility", "hidden");
      .attr("stroke", "#EBEBEB");

    // create bars
    rects
      .enter()
      .append("rect")
      .attr("x", (d, i) => i * 30.5)
      .attr("y", 222)
      .attr("width", 23)
      .attr("transform", "translate(100, 0)")
      .attr("height", (d) => yInfected(d.properties.casosConfirmados))
      .attr("fill", "#FA0067");

    //-------------------------Bar chart end----------------------------//

    this.casesColor = d3
      .scaleLinear()
      .domain([
        0,
        d3.max(featureCollection, (d) => {
          return parseInt(d.properties.casosConfirmados);
        }),
      ])
      .range(["#fff1f7", "#FA0067"]);

    // casesColor per cápita
    this.casesColorPerCapita = d3
      .scaleLinear()
      .domain([
        0,
        d3.max(perCapita, (d) => {
          return parseInt(d.casosPerCapita);
        }),
      ])
      .range(["#fff", "#FFAA25"]);

    // Data for visualize infected population
    this.infectedData = featureCollection.map((d) => {
      return d.properties.casosConfirmados;
    });

    //  Data for visualize cases per cápita
    this.perCapitaData = featureCollection.map((d) => {
      return d.properties.casosPerCapita;
    });

    //  Data for visualize cases per cápita ordered
    this.perCapitaDataOrdered = perCapita.map((d) => {
      return d.casosPerCapita;
    });

    // Define the div for the tooltip
    const tooltip = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .style("opacity", 0)
      .style("transition", ".3s");

    // ----------- create legend -------------------//

    this.yLegend = d3
      .scaleLinear()
      .range([300, 0])
      .domain([
        0,
        Math.ceil(
          d3.max(featureCollection, (d) => {
            return parseInt(d.properties.casosConfirmados);
          }) / 10000
        ) * 10000,
      ]);

    this.yLegendPerCapita = d3
      .scaleLinear()
      .range([300, 0])
      .domain([
        0,
        Math.ceil(
          d3.max(perCapita, (d) => {
            return parseInt(d.casosPerCapita);
          }) / 1000
        ) * 1000,
      ]);

    legendSvgContainer
      .append("g")
      .attr("class", "legend-y-axis")
      .attr("transform", "translate(50,10)")
      .call(d3.axisRight(yLegend));

    legendSvgContainer
      .selectAll("line")
      .attr("class", "legend-line")
      .attr("x2", -50)
      .attr("stroke", "lightGray");

    // ----------- end of legend geometry -------------------//

    // ----------- Map geometry -------------------//
    mapSvgContainer
      .selectAll("g")
      .data(featureCollection)
      .enter()
      .append("path")
      .attr("class", "region")
      .attr("stroke", "#fff")
      .attr("stroke-width", 0.1)
      .attr("d", path)
      .style("fill", function (d, i) {
        return casesColor(infectedData[i]);
      })
      .on("mouseover", function (d) {
        tooltip.transition().duration(200).style("opacity", 1);
        tooltip
          .html(
            `<div class="tooltip-header"> ${
              d.properties.nombreLugar
            }</div><div class="tooltip-row">Habitantes <span class="tooltip-header-total">${d.properties.population
              .toString()
              .replace(
                /(\d)(?=(\d\d\d)+([^\d]|$))/g,
                "$1 "
              )}</span></div><div class="tooltip-row">Diagnosticados <span class="tooltip-header-confirmed">${d.properties.casosConfirmados
              .toString()
              .replace(
                /(\d)(?=(\d\d\d)+([^\d]|$))/g,
                "$1 "
              )}</span></div><div class="tooltip-row">Ingresados UCI <span class="tooltip-header-usi">${d.properties.casosUci
              .toString()
              .replace(
                /(\d)(?=(\d\d\d)+([^\d]|$))/g,
                "$1 "
              )}</span></div><div class="tooltip-row">Fallecidos <span class="tooltip-header-passed">${d.properties.casosFallecidos
              .toString()
              .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ")}</span></div>`
          )
          .style("left", widther > 768 ? d3.event.pageX - 70 + "px" : 20 + "px")
          .style("top", widther > 768 ? d3.event.pageY + 35 + "px" : 20 + "%");
      })
      .on("mouseout", function (d) {
        // console.log(widther);
        tooltip.transition().duration(700).style("opacity", 0);
      });
  })
  .catch(function (error) {
    console.log(error);
  });

// ----------- End of map geometry -------------------//

// ---------------- Function for update map and chart ----------------//
function mapUpdate(dataName) {
  if (dataName === "total") {
    dataName = infectedData;
  }
  if (dataName === "capita") {
    dataName = perCapitaData;
  }

  // // map change
  mapSvgContainer.selectAll(".region").style("fill", function (d, i) {
    if (dataName === infectedData) {
      return casesColor(dataName[i]);
    } else if (dataName === perCapitaData) {
      return casesColorPerCapita(perCapitaData[i]);
    }
  });

  // // chart change
  chartSvgContainer
    .selectAll("rect")
    .transition()
    .duration(600)
    .attr("height", (d, i) => {
      if (dataName === infectedData) {
        return yInfected(dataName[i]);
      } else if (dataName === perCapitaData) {
        return yInfectedPerCapita(perCapitaDataOrdered[i]);
      }
    })
    .attr("fill", dataName === infectedData ? "#fa0067" : "#FFAA25");

  chartSvgContainer
    .selectAll(".chart-label")
    .data(dataName === infectedData ? featureCollection : perCapita)
    // .data(perCapita)
    .text(function (d) {
      return dataName === infectedData ? d.properties.nombreLugar : d.name;
      // return d.name;
    });

  // // axis change
  chartSvgContainer
    .selectAll(".y-axis-barchart")
    .call(dataName === infectedData ? yAxisCall : yAxisCallPerCapita)
    .selectAll("text")
    .attr("y", "-10")
    .attr("x", "15")
    .attr("text-anchor", "start")
    .attr("transform", "rotate(90)translate(-20, -3)");

  // // preserve grid
  chartSvgContainer
    .selectAll(".y-axis-barchart")
    .selectAll("line")
    .attr("y2", 580)
    .attr("stroke", "#EBEBEB")
    .attr("transform", "rotate(90.6)translate(0, 5)");

  // // chart name change
  chartSvgContainer
    .selectAll(".chart-name")
    .text(
      dataName === infectedData
        ? "Casos confirmados"
        : "Casos por 100 000 personas"
    );

  // // legend change
  legendSvgContainer
    .selectAll("rect")
    .style(
      "fill",
      dataName === infectedData ? "url(#gradient)" : "url(#gradient-percapita)"
    );

  legendSvgContainer
    .selectAll(".legend-y-axis")
    .call(d3.axisRight(dataName === infectedData ? yLegend : yLegendPerCapita));

  const ticks = d3.select(".legend-y-axis").selectAll("g");

  ticks.selectAll("line").attr("x2", -50).attr("stroke", "lightGray");
}

// // handle on click event to switch between maps
d3.selectAll(".toggle").on("click", function () {
  let dataName = this.getAttribute("value");
  mapUpdate(dataName);

  if (dataName === "total") {
    sortByCases();
    d3.select(".map-first").classed("map-active", true);
    d3.select(".map-second").classed("map-active", false);
    d3.select(".map-btn-bg").classed("map-left", true);
    d3.select(".map-btn-bg").classed("map-right", false);
  }
  if (dataName === "capita") {
    sortByCapita();
    d3.select(".map-first").classed("map-active", false);
    d3.select(".map-second").classed("map-active", true);
    d3.select(".map-btn-bg").classed("map-right", true);
    d3.select(".map-btn-bg").classed("map-left", false);
  }
});
