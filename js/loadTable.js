function loadTableData(fecha) {
	url = 'https://covid19.secuoyas.io/api/v1/es/ccaa/?fecha=' + fecha;
	$.getJSON(url, function (json) {
		var datos = json.timeline[0].regiones;
		for (i in datos) {
			if ( datos[i].data.casosRecuperados == '-1' || datos[i].data.casosRecuperados == void 0 || typeof datos[i].data.casosRecuperados == 'undefined' ) {
				$('#tablafechaTimeline-' + fecha).append(
					'<tr><td class="cell-comunidad">' +
						datos[i].nombreLugar +
						'</td><td class="cell-recuperados">0' +
						'</td><td class="cell-fallecidos">' +
						datos[i].data.casosFallecidos +
						'</td><td class="cell-confirmados">' +
						datos[i].data.casosConfirmadosDiario +
						'</td><td class="cell-total">' +
						datos[i].data.casosConfirmados +
						'</td></tr>'
				);
			} else {
				$('#tablafechaTimeline-' + fecha).append(
					'<tr><td class="cell-comunidad">' +
						datos[i].nombreLugar +
						'</td><td class="cell-recuperados">' +
						datos[i].data.casosRecuperados +
						'</td><td class="cell-fallecidos">' +
						datos[i].data.casosFallecidos +
						'</td><td class="cell-confirmados">' +
						datos[i].data.casosConfirmadosDiario +
						'</td><td class="cell-total">' +
						datos[i].data.casosConfirmados +
						'</td></tr>'
				);
			}
		}
	});
}

var fecha;
var fechaTimeline = [];
$(document).ready(function () {
	$('[id^=tablafechaTimeline]').each(function (index) {
		var idFecha = $(this).attr('id');
		fecha = idFecha.substring(19);
		loadTableData(fecha);
	});
});
