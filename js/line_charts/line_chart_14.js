// append the svg obgect to the body of the page
const chartMadrid = d3
  .select("#line-chart-container-14")
  .append("svg")
  .attr("class", "Madrid")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

Promise.all([
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?codigo=ES-MD")
])
  .then(file => {
    const dataForRenderMadrid = file[0];
    const timelineMadrid = dataForRenderMadrid.timeline;

    dataArr = [];

    // format the data
    timelineMadrid.forEach(function(d, i) {
      dataArr.push(d.regiones[0].data.casosConfirmados);
      d.fecha = parseTime(d.fecha);
    });
    // Add label
    chartMadrid
      .append("text")
      .attr("class", "comunity-name")
      .attr("transform", "translate(0, -85)")
      .text(dataForRenderMadrid.trace.info.nombreLugar);

    //---------- create array for draw a curves of reference -------------//
    const getArr = (potencia, periodo, i, arr) => {
      const key = ["growthTwo", "growthThree", "growthFour", "growthFive"];
      timelineMadrid.map((item, idx) => {
        arr.push({ [key[i]]: Math.pow(potencia, idx / periodo) });
      });
    };

    // for (let idx = 0; idx <= 160; idx++) {
    //   console.log(Math.pow(2, idx / 3));
    // }

    // ---------------- Get the las itme in array of cases ---------------- //
    const casesArrMadrid = [];
    timelineMadrid.map(item => {
      casesArrMadrid.push(item.regiones[0].data.casosConfirmados);
    });

    this.largestNumMadrid = Math.max(...casesArrMadrid);
    // console.log(largestNumMadrid);

    //// --------- get data for reference curves ---------- ////
    const curveTwo = [];
    getArr(2, 2, 0, curveTwo);

    const curveThree = [];
    getArr(2, 3, 1, curveThree);

    const curveFour = [];
    getArr(2, 4, 2, curveFour);

    const curveFive = [];
    getArr(2, 5, 3, curveFive);

    //// --------- end of get data for reference curves ---------- ////

    //// --------- assign reference curve data to array of cases ---------- ////
    timelineMadrid.map((d, i) => {
      Object.assign(d, curveTwo[i]);
    });

    timelineMadrid.map((d, i) => {
      Object.assign(d, curveThree[i]);
    });

    timelineMadrid.map((d, i) => {
      Object.assign(d, curveFour[i]);
    });

    timelineMadrid.map((d, i) => {
      Object.assign(d, curveFive[i]);
    });
    // console.log(timelineMadrid);
    //// --------- set the ranges ---------- ////

    this.xFecha = d3
      .scaleTime()
      .range([0, width])
      .domain(
        d3.extent(timelineMadrid, function(d) {
          return d.fecha;
        })
      );

    //------------ set y linear rande equal to 10 000 or 30 000 ----------//
    this.yLinearMadrid = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        parseInt(largestNumMadrid) < 10000
          ? 10000
          : parseInt(largestNumMadrid) > 10000 &&
            parseInt(largestNumMadrid) < 30000
          ? 30000
          : parseInt(largestNumMadrid) > 30000 &&
            parseInt(largestNumMadrid) < 60000
          ? 60000
          : largestNumMadrid
      ]);

    //------------ set y log rande equal to 10 000 or 30 000 ----------//
    this.yLogMadrid = d3
      // .scaleSymlog()
      .scaleLog()
      .base(10)
      .range([height, 0])
      .domain([1, parseInt(largestNumMadrid) < 10000 ? 10000 : 100000]);

    //// --------- end the ranges set ---------- ////

    //// --------- define the line ---------- ////
    // define the  linear curve
    this.valuelineLinearMadrid = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearMadrid(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    // define the  logarithmic curve
    this.valuelineLogMadrid = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogMadrid(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    //---------------- reference curves ---------------------//
    // define the linear curve for eachTwo data
    this.dataEveryTwoMadridLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumMadrid < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumMadrid < 10000) {
          return yLinearMadrid(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return yLinearMadrid(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return yLinearMadrid(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachTwo data
    this.dataEveryTwoMadridLog = d3
      .line()
      .defined(function(d) {
        if (largestNumMadrid < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumMadrid < 10000) {
          return yLogMadrid(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return yLogMadrid(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return yLogMadrid(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachThree data
    this.dataEveryThreeMadridLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumMadrid < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumMadrid < 10000) {
          return yLinearMadrid(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return yLinearMadrid(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return yLinearMadrid(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachThree data
    this.dataEveryThreeMadridLog = d3
      .line()
      .defined(function(d) {
        if (largestNumMadrid < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumMadrid < 10000) {
          return yLogMadrid(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumMadrid > 10000 && largestNumMadrid < 30000) {
          return yLogMadrid(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumMadrid > 30000 && largestNumMadrid < 60000) {
          return yLogMadrid(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFour data
    this.dataEveryFourMadridLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearMadrid(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFour data
    this.dataEveryFourMadridLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogMadrid(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFivedata
    this.dataEveryFiveMadridLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearMadrid(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFive data
    this.dataEveryFiveMadridLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogMadrid(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(0));
    //// --------- end of lines definition ---------- ////

    //// --------- caling the axis --------- ////
    this.yAxisCallLinearMadrid = d3.axisLeft(yLinearMadrid).ticks(4);
    // this.yAxisCallLinearMadrid = d3.axisLeft(yLinearMadrid).ticks(4);
    this.yAxisCallLogMadrid = d3
      .axisLeft(yLogMadrid)
      // .ticks(4)
      .tickValues(
        largestNumMadrid < 10000
          ? [1, 10, 100, 1000, 10000]
          : [1, 10, 100, 1000, 10000, 100000]
      )
      .tickArguments([0, ",.0f"]);

    // Add the X Axis
    chartMadrid
      .append("g")
      .attr("class", "x-axis-linechart")
      .attr("transform", `translate(0,${height + 10})`)
      .call(d3.axisBottom(xFecha));

    // Add the Y Axis
    chartMadrid
      .append("g")
      .attr("class", "y-axis-linechart")
      .call(yAxisCallLinearMadrid);

    // Create horizontal grid
    chartMadrid
      .selectAll(".y-axis-linechart")
      .selectAll(".tick")
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");

    // -------------- Create curves ---------------- //

    // double each two days curve
    chartMadrid
      .append("path")
      .data([timelineMadrid])
      .attr("class", "path-line-two")
      .style("stroke", "#19e99e")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryTwoMadridLinear);

    // double each three days curve
    chartMadrid
      .append("path")
      .data([timelineMadrid])
      .attr("class", "path-line-three")
      .style("stroke", "#ffaa25")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryThreeMadridLinear);

    // double each four days curve
    chartMadrid
      .append("path")
      .data([timelineMadrid])
      .attr("class", "path-line-four")
      .style("stroke", "#a950cf")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFourMadridLinear);

    // double each five days curve
    chartMadrid
      .append("path")
      .data([timelineMadrid])
      .attr("class", "path-line-five")
      .style("stroke", "#82afff")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFiveMadridLinear);

    //// --------- add curve for Madrid cases ---------- ////
    chartMadrid
      .append("path")
      .data([timelineMadrid])
      .attr("class", "path-line")
      .style("stroke", "#fa0067")
      .style("fill", "none")
      .attr("d", valuelineLinearMadrid);

    //// --------- end of y line adding ---------- ////

    //// --------- add circles ---------- ////
    chartMadrid
      .append("g")
      .attr("class", "path-line-circle")
      .selectAll("circle")
      .data(timelineMadrid)
      .enter()
      .append("circle")
      .attr("r", widther > 500 ? 3 : 2)
      .attr("cx", function(d) {
        return xFecha(d.fecha);
      })
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearMadrid(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      })
      .style("fill", "#fa0067");

    chartMadrid.selectAll("circle").style("display", function(d) {
      return d.regiones[0].data.casosConfirmados === -1 ? "none" : null;
    });
    //// --------- end of adding circles ---------- ////

    //// --------- add legend ---------- ////

    const legendData = [
      { color: "#19e99e", legendName: "2 días" },
      { color: "#ffaa25", legendName: "3 días" },
      { color: "#a950cf", legendName: "4 días" },
      { color: "#82afff", legendName: "5 días" }
    ];

    const legend = chartMadrid.append("g").attr("class", "line-chart-legend");

    legend
      .selectAll("g")
      .data(legendData)
      .enter()
      .append("g")
      .attr("class", "legend-item")
      .append("rect")
      .attr("class", "legend-rect")
      .attr("y", width - 305)
      .attr("x", function(d, i) {
        return (i + 1.3) * 65;
      })
      .attr("width", 18)
      .attr("height", 2)
      .style("fill", function(d) {
        return d.color;
      });

    // legend.attr("transform", "translate(58,-160)");

    legend
      .append("g")
      .attr("class", "legend-title")
      .append("text")
      .text("Velocidad de duplicación")
      .attr("transform", `translate(-60, ${width - 300})`);

    legend
      .selectAll(".legend-item")
      .append("text")
      .attr("y", width - 300)
      .attr("x", function(d, i) {
        return (i + 1.65) * 65;
      })
      .text(function(d) {
        return d.legendName;
      });

    //// --------- end of adding legend---------- ////
  })
  .catch(function(error) {
    console.log(error);
  });

//// --------- update charts function ---------- ////
function chartUpdateMadrid(dataName) {
  if (dataName === "logarithmic") {
    chartMadrid
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveMadridLog);

    chartMadrid
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourMadridLog);

    chartMadrid
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeMadridLog);

    chartMadrid
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoMadridLog);

    chartMadrid
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLogMadrid);

    chartMadrid
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLogMadrid(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });

    chartMadrid
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLogMadrid)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
  if (dataName === "linear") {
    chartMadrid
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveMadridLinear);

    chartMadrid
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourMadridLinear);

    chartMadrid
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeMadridLinear);

    chartMadrid
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoMadridLinear);

    chartMadrid
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLinearMadrid);

    chartMadrid
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearMadrid(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });
    chartMadrid
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLinearMadrid)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
}

//// --------- end of update function ---------- ////
