"use strict";
// set the dimensions and margins of the graph
const widther = window.outerWidth;
const margin = { top: 100, right: 20, bottom: 30, left: 50 },
  width = widther > 500 ? 490 - margin.left - margin.right : 270,
  height = widther > 500 ? 400 - margin.top - margin.bottom : 220;

// parse the fecha
const parseTime = d3.timeParse("%Y-%m-%d");

//// --------- handle on click event to switch between log and linear ---------- ////

d3.selectAll(".option").on("click", function () {
  let dataName = this.getAttribute("value");
  chartUpdateAndalucia(dataName);
  chartUpdateAragon(dataName);
  chartUpdateAsturias(dataName);
  chartUpdateBaleares(dataName);
  chartUpdateCanarias(dataName);
  chartUpdateCantabria(dataName);
  chartUpdateCastillaLaMancha(dataName);
  chartUpdateCastillaLeon(dataName);
  chartUpdateCatalunya(dataName);
  chartUpdateCeuta(dataName);
  chartUpdateValencia(dataName);
  chartUpdateExtremadura(dataName);
  chartUpdateGalicia(dataName);
  chartUpdateMadrid(dataName);
  chartUpdateMelilla(dataName);
  chartUpdateMurcia(dataName);
  chartUpdateNavarra(dataName);
  chartUpdatePaisVasco(dataName);
  chartUpdateRioja(dataName);

  if (dataName === "linear") {
    d3.select(".linechart-first").classed("active", true);
    d3.select(".linechart-second").classed("active", false);
    d3.select(".btn-bg").classed("left", true);
    d3.select(".btn-bg").classed("right", false);
  }
  if (dataName === "logarithmic") {
    d3.select(".linechart-first").classed("active", false);
    d3.select(".linechart-second").classed("active", true);
    d3.select(".btn-bg").classed("right", true);
    d3.select(".btn-bg").classed("left", false);
  }
});
//// --------- end of hadndle switch function ---------- ////

// Hacemos llamada a datos para pintar la fecha de ultima actualización en header de pagina de dashboard
Promise.all([
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?ultimodia=true"),
])
  .then((data) => {
    const lastUpdateDate = data[0].timeline[0].fecha;
    const lastUpdateDay = lastUpdateDate.slice(8, 11);
    const lastUpdateMonth = lastUpdateDate.slice(5, 7);
    const lastUpdateYear = lastUpdateDate.slice(0, 4);
    const dayEl = document.querySelector("#lastUpdateDay");
    const monthEl = document.querySelector("#lastUpdateMonth");
    const yearEl = document.querySelector("#lastUpdateYear");

    dayEl.innerHTML = lastUpdateDay;
    monthEl.innerHTML = lastUpdateMonth;
    yearEl.innerHTML = lastUpdateYear;
  })
  .catch(function (error) {
    console.log(error);
  });
