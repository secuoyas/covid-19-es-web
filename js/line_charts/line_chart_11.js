// append the svg obgect to the body of the page
const chartValencia = d3
  .select("#line-chart-container-11")
  .append("svg")
  .attr("class", "Valencia")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

Promise.all([
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?codigo=ES-VC")
])
  .then(file => {
    const dataForRenderValencia = file[0];
    const timelineValencia = dataForRenderValencia.timeline;

    dataArr = [];

    // format the data
    timelineValencia.forEach(function(d, i) {
      dataArr.push(d.regiones[0].data.casosConfirmados);
      d.fecha = parseTime(d.fecha);
    });
    // Add label
    chartValencia
      .append("text")
      .attr("class", "comunity-name")
      .attr("transform", "translate(0, -85)")
      .text(dataForRenderValencia.trace.info.nombreLugar);

    //---------- create array for draw a curves of reference -------------//
    const getArr = (potencia, periodo, i, arr) => {
      const key = ["growthTwo", "growthThree", "growthFour", "growthFive"];
      timelineValencia.map((item, idx) => {
        arr.push({ [key[i]]: Math.pow(potencia, idx / periodo) });
      });
    };

    // for (let idx = 0; idx <= 160; idx++) {
    //   console.log(Math.pow(2, idx / 3));
    // }

    // ---------------- Get the las itme in array of cases ---------------- //
    const casesArrValencia = [];
    timelineValencia.map(item => {
      casesArrValencia.push(item.regiones[0].data.casosConfirmados);
    });

    this.largestNumValencia = Math.max(...casesArrValencia);
    // console.log(largestNumValencia);

    //// --------- get data for reference curves ---------- ////
    const curveTwo = [];
    getArr(2, 2, 0, curveTwo);

    const curveThree = [];
    getArr(2, 3, 1, curveThree);

    const curveFour = [];
    getArr(2, 4, 2, curveFour);

    const curveFive = [];
    getArr(2, 5, 3, curveFive);

    //// --------- end of get data for reference curves ---------- ////

    //// --------- assign reference curve data to array of cases ---------- ////
    timelineValencia.map((d, i) => {
      Object.assign(d, curveTwo[i]);
    });

    timelineValencia.map((d, i) => {
      Object.assign(d, curveThree[i]);
    });

    timelineValencia.map((d, i) => {
      Object.assign(d, curveFour[i]);
    });

    timelineValencia.map((d, i) => {
      Object.assign(d, curveFive[i]);
    });
    // console.log(timelineValencia);
    //// --------- set the ranges ---------- ////

    this.xFecha = d3
      .scaleTime()
      .range([0, width])
      .domain(
        d3.extent(timelineValencia, function(d) {
          return d.fecha;
        })
      );

    //------------ set y linear rande equal to 10 000 or 30 000 ----------//
    this.yLinearValencia = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        parseInt(largestNumValencia) < 10000
          ? 10000
          : parseInt(largestNumValencia) > 10000 &&
            parseInt(largestNumValencia) < 30000
          ? 30000
          : parseInt(largestNumValencia) > 30000 &&
            parseInt(largestNumValencia) < 60000
          ? 60000
          : largestNumValencia
      ]);

    //------------ set y log rande equal to 10 000 or 30 000 ----------//
    this.yLogValencia = d3
      // .scaleSymlog()
      .scaleLog()
      .base(10)
      .range([height, 0])
      .domain([1, parseInt(largestNumValencia) < 10000 ? 10000 : 100000]);

    //// --------- end the ranges set ---------- ////

    //// --------- define the line ---------- ////
    // define the  linear curve
    this.valuelineLinearValencia = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearValencia(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    // define the  logarithmic curve
    this.valuelineLogValencia = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogValencia(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    //---------------- reference curves ---------------------//
    // define the linear curve for eachTwo data
    this.dataEveryTwoValenciaLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumValencia < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumValencia < 10000) {
          return yLinearValencia(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return yLinearValencia(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return yLinearValencia(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachTwo data
    this.dataEveryTwoValenciaLog = d3
      .line()
      .defined(function(d) {
        if (largestNumValencia < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumValencia < 10000) {
          return yLogValencia(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return yLogValencia(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return yLogValencia(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachThree data
    this.dataEveryThreeValenciaLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumValencia < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumValencia < 10000) {
          return yLinearValencia(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return yLinearValencia(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return yLinearValencia(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachThree data
    this.dataEveryThreeValenciaLog = d3
      .line()
      .defined(function(d) {
        if (largestNumValencia < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumValencia < 10000) {
          return yLogValencia(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumValencia > 10000 && largestNumValencia < 30000) {
          return yLogValencia(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumValencia > 30000 && largestNumValencia < 60000) {
          return yLogValencia(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFour data
    this.dataEveryFourValenciaLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearValencia(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFour data
    this.dataEveryFourValenciaLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogValencia(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFivedata
    this.dataEveryFiveValenciaLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearValencia(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFive data
    this.dataEveryFiveValenciaLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogValencia(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(0));
    //// --------- end of lines definition ---------- ////

    //// --------- caling the axis --------- ////
    this.yAxisCallLinearValencia = d3.axisLeft(yLinearValencia).ticks(4);
    // this.yAxisCallLinearValencia = d3.axisLeft(yLinearValencia).ticks(4);
    this.yAxisCallLogValencia = d3
      .axisLeft(yLogValencia)
      // .ticks(4)
      .tickValues(
        largestNumValencia < 10000
          ? [1, 10, 100, 1000, 10000]
          : [1, 10, 100, 1000, 10000, 100000]
      )
      .tickArguments([0, ",.0f"]);

    // Add the X Axis
    chartValencia
      .append("g")
      .attr("class", "x-axis-linechart")
      .attr("transform", `translate(0,${height + 10})`)
      .call(d3.axisBottom(xFecha));

    // Add the Y Axis
    chartValencia
      .append("g")
      .attr("class", "y-axis-linechart")
      .call(yAxisCallLinearValencia);

    // Create horizontal grid
    chartValencia
      .selectAll(".y-axis-linechart")
      .selectAll(".tick")
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");

    // -------------- Create curves ---------------- //

    // double each two days curve
    chartValencia
      .append("path")
      .data([timelineValencia])
      .attr("class", "path-line-two")
      .style("stroke", "#19e99e")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryTwoValenciaLinear);

    // double each three days curve
    chartValencia
      .append("path")
      .data([timelineValencia])
      .attr("class", "path-line-three")
      .style("stroke", "#ffaa25")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryThreeValenciaLinear);

    // double each four days curve
    chartValencia
      .append("path")
      .data([timelineValencia])
      .attr("class", "path-line-four")
      .style("stroke", "#a950cf")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFourValenciaLinear);

    // double each five days curve
    chartValencia
      .append("path")
      .data([timelineValencia])
      .attr("class", "path-line-five")
      .style("stroke", "#82afff")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFiveValenciaLinear);

    //// --------- add curve for Valencia cases ---------- ////
    chartValencia
      .append("path")
      .data([timelineValencia])
      .attr("class", "path-line")
      .style("stroke", "#fa0067")
      .style("fill", "none")
      .attr("d", valuelineLinearValencia);

    //// --------- end of y line adding ---------- ////

    //// --------- add circles ---------- ////
    chartValencia
      .append("g")
      .attr("class", "path-line-circle")
      .selectAll("circle")
      .data(timelineValencia)
      .enter()
      .append("circle")
      .attr("r", widther > 500 ? 3 : 2)
      .attr("cx", function(d) {
        return xFecha(d.fecha);
      })
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearValencia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      })
      .style("fill", "#fa0067");

    chartValencia.selectAll("circle").style("display", function(d) {
      return d.regiones[0].data.casosConfirmados === -1 ? "none" : null;
    });
    //// --------- end of adding circles ---------- ////

    //// --------- add legend ---------- ////

    const legendData = [
      { color: "#19e99e", legendName: "2 días" },
      { color: "#ffaa25", legendName: "3 días" },
      { color: "#a950cf", legendName: "4 días" },
      { color: "#82afff", legendName: "5 días" }
    ];

    const legend = chartValencia.append("g").attr("class", "line-chart-legend");

    legend
      .selectAll("g")
      .data(legendData)
      .enter()
      .append("g")
      .attr("class", "legend-item")
      .append("rect")
      .attr("class", "legend-rect")
      .attr("y", width - 305)
      .attr("x", function(d, i) {
        return (i + 1.3) * 65;
      })
      .attr("width", 18)
      .attr("height", 2)
      .style("fill", function(d) {
        return d.color;
      });

    // legend.attr("transform", "translate(58,-160)");

    legend
      .append("g")
      .attr("class", "legend-title")
      .append("text")
      .text("Velocidad de duplicación")
      .attr("transform", `translate(-60, ${width - 300})`);

    legend
      .selectAll(".legend-item")
      .append("text")
      .attr("y", width - 300)
      .attr("x", function(d, i) {
        return (i + 1.65) * 65;
      })
      .text(function(d) {
        return d.legendName;
      });

    //// --------- end of adding legend---------- ////
  })
  .catch(function(error) {
    console.log(error);
  });

//// --------- update charts function ---------- ////
function chartUpdateValencia(dataName) {
  if (dataName === "logarithmic") {
    chartValencia
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveValenciaLog);

    chartValencia
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourValenciaLog);

    chartValencia
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeValenciaLog);

    chartValencia
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoValenciaLog);

    chartValencia
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLogValencia);

    chartValencia
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLogValencia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });

    chartValencia
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLogValencia)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
  if (dataName === "linear") {
    chartValencia
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveValenciaLinear);

    chartValencia
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourValenciaLinear);

    chartValencia
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeValenciaLinear);

    chartValencia
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoValenciaLinear);

    chartValencia
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLinearValencia);

    chartValencia
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearValencia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });
    chartValencia
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLinearValencia)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
}

//// --------- end of update function ---------- ////
