// append the svg obgect to the body of the page
const chartAndalucia = d3
  .select("#line-chart-container-1")
  .append("svg")
  .attr("class", "Andalucia")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

Promise.all([
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?codigo=ES-AN")
])
  .then(file => {
    const dataForRenderAndalucia = file[0];
    const timelineAndalucia = dataForRenderAndalucia.timeline;

    dataArr = [];

    // format the data
    timelineAndalucia.forEach(function(d, i) {
      dataArr.push(d.regiones[0].data.casosConfirmados);
      d.fecha = parseTime(d.fecha);
    });
    // Add label
    chartAndalucia
      .append("text")
      .attr("class", "comunity-name")
      .attr("transform", "translate(0, -85)")
      .text(dataForRenderAndalucia.trace.info.nombreLugar);

    //---------- create array for draw a curves of reference -------------//
    const getArr = (potencia, periodo, i, arr) => {
      const key = ["growthTwo", "growthThree", "growthFour", "growthFive"];
      timelineAndalucia.map((item, idx) => {
        arr.push({ [key[i]]: Math.pow(potencia, idx / periodo) });
      });
    };

    // for (let idx = 0; idx <= 160; idx++) {
    //   console.log(Math.pow(2, idx / 3));
    // }

    // ---------------- Get the las itme in array of cases ---------------- //
    const casesArrAndalucia = [];
    timelineAndalucia.map(item => {
      casesArrAndalucia.push(item.regiones[0].data.casosConfirmados);
    });

    this.largestNumAndalucia = Math.max(...casesArrAndalucia);
    // console.log(largestNumAndalucia);

    //// --------- get data for reference curves ---------- ////
    const curveTwo = [];
    getArr(2, 2, 0, curveTwo);

    const curveThree = [];
    getArr(2, 3, 1, curveThree);

    const curveFour = [];
    getArr(2, 4, 2, curveFour);

    const curveFive = [];
    getArr(2, 5, 3, curveFive);

    //// --------- end of get data for reference curves ---------- ////

    //// --------- assign reference curve data to array of cases ---------- ////
    timelineAndalucia.map((d, i) => {
      Object.assign(d, curveTwo[i]);
    });

    timelineAndalucia.map((d, i) => {
      Object.assign(d, curveThree[i]);
    });

    timelineAndalucia.map((d, i) => {
      Object.assign(d, curveFour[i]);
    });

    timelineAndalucia.map((d, i) => {
      Object.assign(d, curveFive[i]);
    });
    // console.log(timelineAndalucia);
    //// --------- set the ranges ---------- ////

    this.xFecha = d3
      .scaleTime()
      .range([0, width])
      .domain(
        d3.extent(timelineAndalucia, function(d) {
          return d.fecha;
        })
      );

    //------------ set y linear rande equal to 10 000 or 30 000 ----------//
    this.yLinearAndalucia = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        parseInt(largestNumAndalucia) < 10000
          ? 10000
          : parseInt(largestNumAndalucia) > 10000 &&
            parseInt(largestNumAndalucia) < 30000
          ? 30000
          : parseInt(largestNumAndalucia) > 30000 &&
            parseInt(largestNumAndalucia) < 60000
          ? 60000
          : largestNumAndalucia
      ]);

    //------------ set y log rande equal to 10 000 or 30 000 ----------//
    this.yLogAndalucia = d3
      // .scaleSymlog()
      .scaleLog()
      .base(10)
      .range([height, 0])
      .domain([1, parseInt(largestNumAndalucia) < 10000 ? 10000 : 100000]);

    //// --------- end the ranges set ---------- ////

    //// --------- define the line ---------- ////
    // define the  linear curve
    this.valuelineLinearAndalucia = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearAndalucia(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    // define the  logarithmic curve
    this.valuelineLogAndalucia = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogAndalucia(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    //---------------- reference curves ---------------------//
    // define the linear curve for eachTwo data
    this.dataEveryTwoAndaluciaLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumAndalucia < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumAndalucia < 10000) {
          return yLinearAndalucia(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return yLinearAndalucia(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return yLinearAndalucia(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachTwo data
    this.dataEveryTwoAndaluciaLog = d3
      .line()
      .defined(function(d) {
        if (largestNumAndalucia < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumAndalucia < 10000) {
          return yLogAndalucia(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return yLogAndalucia(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return yLogAndalucia(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachThree data
    this.dataEveryThreeAndaluciaLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumAndalucia < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumAndalucia < 10000) {
          return yLinearAndalucia(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return yLinearAndalucia(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return yLinearAndalucia(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachThree data
    this.dataEveryThreeAndaluciaLog = d3
      .line()
      .defined(function(d) {
        if (largestNumAndalucia < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumAndalucia < 10000) {
          return yLogAndalucia(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumAndalucia > 10000 && largestNumAndalucia < 30000) {
          return yLogAndalucia(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumAndalucia > 30000 && largestNumAndalucia < 60000) {
          return yLogAndalucia(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFour data
    this.dataEveryFourAndaluciaLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearAndalucia(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFour data
    this.dataEveryFourAndaluciaLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogAndalucia(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFivedata
    this.dataEveryFiveAndaluciaLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearAndalucia(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFive data
    this.dataEveryFiveAndaluciaLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogAndalucia(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(0));
    //// --------- end of lines definition ---------- ////

    //// --------- caling the axis --------- ////
    this.yAxisCallLinearAndalucia = d3.axisLeft(yLinearAndalucia).ticks(4);
    // this.yAxisCallLinearAndalucia = d3.axisLeft(yLinearAndalucia).ticks(4);
    this.yAxisCallLogAndalucia = d3
      .axisLeft(yLogAndalucia)
      // .ticks(4)
      .tickValues(
        largestNumAndalucia < 10000
          ? [1, 10, 100, 1000, 10000]
          : [1, 10, 100, 1000, 10000, 100000]
      )
      .tickArguments([0, ",.0f"]);

    // Add the X Axis
    chartAndalucia
      .append("g")
      .attr("class", "x-axis-linechart")
      .attr("transform", `translate(0,${height + 10})`)
      .call(d3.axisBottom(xFecha));

    // Add the Y Axis
    chartAndalucia
      .append("g")
      .attr("class", "y-axis-linechart")
      .call(yAxisCallLinearAndalucia);

    // Create horizontal grid
    chartAndalucia
      .selectAll(".y-axis-linechart")
      .selectAll(".tick")
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");

    // -------------- Create curves ---------------- //

    // double each two days curve
    chartAndalucia
      .append("path")
      .data([timelineAndalucia])
      .attr("class", "path-line-two")
      .style("stroke", "#19e99e")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryTwoAndaluciaLinear);

    // double each three days curve
    chartAndalucia
      .append("path")
      .data([timelineAndalucia])
      .attr("class", "path-line-three")
      .style("stroke", "#ffaa25")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryThreeAndaluciaLinear);

    // double each four days curve
    chartAndalucia
      .append("path")
      .data([timelineAndalucia])
      .attr("class", "path-line-four")
      .style("stroke", "#a950cf")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFourAndaluciaLinear);

    // double each five days curve
    chartAndalucia
      .append("path")
      .data([timelineAndalucia])
      .attr("class", "path-line-five")
      .style("stroke", "#82afff")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFiveAndaluciaLinear);

    //// --------- add curve for Andalucia cases ---------- ////
    chartAndalucia
      .append("path")
      .data([timelineAndalucia])
      .attr("class", "path-line")
      .style("stroke", "#fa0067")
      .style("fill", "none")
      .attr("d", valuelineLinearAndalucia);

    //// --------- end of y line adding ---------- ////

    //// --------- add circles ---------- ////
    chartAndalucia
      .append("g")
      .attr("class", "path-line-circle")
      .selectAll("circle")
      .data(timelineAndalucia)
      .enter()
      .append("circle")
      .attr("r", widther > 500 ? 3 : 2)
      .attr("cx", function(d) {
        return xFecha(d.fecha);
      })
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearAndalucia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      })
      .style("fill", "#fa0067");

    chartAndalucia.selectAll("circle").style("display", function(d) {
      return d.regiones[0].data.casosConfirmados === -1 ? "none" : null;
    });
    //// --------- end of adding circles ---------- ////

    //// --------- add legend ---------- ////

    const legendData = [
      { color: "#19e99e", legendName: "2 días" },
      { color: "#ffaa25", legendName: "3 días" },
      { color: "#a950cf", legendName: "4 días" },
      { color: "#82afff", legendName: "5 días" }
    ];

    const legend = chartAndalucia
      .append("g")
      .attr("class", "line-chart-legend");

    legend
      .selectAll("g")
      .data(legendData)
      .enter()
      .append("g")
      .attr("class", "legend-item")
      .append("rect")
      .attr("class", "legend-rect")
      .attr("y", width - 305)
      .attr("x", function(d, i) {
        return (i + 1.3) * 65;
      })
      .attr("width", 18)
      .attr("height", 2)
      .style("fill", function(d) {
        return d.color;
      });

    // legend.attr("transform", "translate(58,-160)");

    legend
      .append("g")
      .attr("class", "legend-title")
      .append("text")
      .text("Velocidad de duplicación")
      .attr("transform", `translate(-60, ${width - 300})`);

    legend
      .selectAll(".legend-item")
      .append("text")
      .attr("y", width - 300)
      .attr("x", function(d, i) {
        return (i + 1.65) * 65;
      })
      .text(function(d) {
        return d.legendName;
      });

    //// --------- end of adding legend---------- ////
  })
  .catch(function(error) {
    console.log(error);
  });

//// --------- update charts function ---------- ////
function chartUpdateAndalucia(dataName) {
  if (dataName === "logarithmic") {
    chartAndalucia
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveAndaluciaLog);

    chartAndalucia
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourAndaluciaLog);

    chartAndalucia
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeAndaluciaLog);

    chartAndalucia
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoAndaluciaLog);

    chartAndalucia
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLogAndalucia);

    chartAndalucia
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLogAndalucia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });

    chartAndalucia
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLogAndalucia)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
  if (dataName === "linear") {
    chartAndalucia
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFiveAndaluciaLinear);

    chartAndalucia
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourAndaluciaLinear);

    chartAndalucia
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreeAndaluciaLinear);

    chartAndalucia
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoAndaluciaLinear);

    chartAndalucia
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLinearAndalucia);

    chartAndalucia
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearAndalucia(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });
    chartAndalucia
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLinearAndalucia)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
}

//// --------- end of update function ---------- ////
