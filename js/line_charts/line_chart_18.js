// append the svg obgect to the body of the page
const chartPaisVasco = d3
  .select("#line-chart-container-18")
  .append("svg")
  .attr("class", "PaisVasco")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

Promise.all([
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?codigo=ES-PV")
])
  .then(file => {
    const dataForRenderPaisVasco = file[0];
    const timelinePaisVasco = dataForRenderPaisVasco.timeline;

    dataArr = [];

    // format the data
    timelinePaisVasco.forEach(function(d, i) {
      dataArr.push(d.regiones[0].data.casosConfirmados);
      d.fecha = parseTime(d.fecha);
    });
    // Add label
    chartPaisVasco
      .append("text")
      .attr("class", "comunity-name")
      .attr("transform", "translate(0, -85)")
      .text(dataForRenderPaisVasco.trace.info.nombreLugar);

    //---------- create array for draw a curves of reference -------------//
    const getArr = (potencia, periodo, i, arr) => {
      const key = ["growthTwo", "growthThree", "growthFour", "growthFive"];
      timelinePaisVasco.map((item, idx) => {
        arr.push({ [key[i]]: Math.pow(potencia, idx / periodo) });
      });
    };

    // for (let idx = 0; idx <= 160; idx++) {
    //   console.log(Math.pow(2, idx / 3));
    // }

    // ---------------- Get the las itme in array of cases ---------------- //
    const casesArrPaisVasco = [];
    timelinePaisVasco.map(item => {
      casesArrPaisVasco.push(item.regiones[0].data.casosConfirmados);
    });

    this.largestNumPaisVasco = Math.max(...casesArrPaisVasco);
    // console.log(largestNumPaisVasco);

    //// --------- get data for reference curves ---------- ////
    const curveTwo = [];
    getArr(2, 2, 0, curveTwo);

    const curveThree = [];
    getArr(2, 3, 1, curveThree);

    const curveFour = [];
    getArr(2, 4, 2, curveFour);

    const curveFive = [];
    getArr(2, 5, 3, curveFive);

    //// --------- end of get data for reference curves ---------- ////

    //// --------- assign reference curve data to array of cases ---------- ////
    timelinePaisVasco.map((d, i) => {
      Object.assign(d, curveTwo[i]);
    });

    timelinePaisVasco.map((d, i) => {
      Object.assign(d, curveThree[i]);
    });

    timelinePaisVasco.map((d, i) => {
      Object.assign(d, curveFour[i]);
    });

    timelinePaisVasco.map((d, i) => {
      Object.assign(d, curveFive[i]);
    });
    // console.log(timelinePaisVasco);
    //// --------- set the ranges ---------- ////

    this.xFecha = d3
      .scaleTime()
      .range([0, width])
      .domain(
        d3.extent(timelinePaisVasco, function(d) {
          return d.fecha;
        })
      );

    //------------ set y linear rande equal to 10 000 or 30 000 ----------//
    this.yLinearPaisVasco = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        parseInt(largestNumPaisVasco) < 10000
          ? 10000
          : parseInt(largestNumPaisVasco) > 10000 &&
            parseInt(largestNumPaisVasco) < 30000
          ? 30000
          : parseInt(largestNumPaisVasco) > 30000 &&
            parseInt(largestNumPaisVasco) < 60000
          ? 60000
          : largestNumPaisVasco
      ]);

    //------------ set y log rande equal to 10 000 or 30 000 ----------//
    this.yLogPaisVasco = d3
      // .scaleSymlog()
      .scaleLog()
      .base(10)
      .range([height, 0])
      .domain([1, parseInt(largestNumPaisVasco) < 10000 ? 10000 : 100000]);

    //// --------- end the ranges set ---------- ////

    //// --------- define the line ---------- ////
    // define the  linear curve
    this.valuelineLinearPaisVasco = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearPaisVasco(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    // define the  logarithmic curve
    this.valuelineLogPaisVasco = d3
      .line()
      .defined(function(d) {
        return d.regiones[0].data.casosConfirmados !== -1;
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogPaisVasco(
          parseInt(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          )
        );
      });

    //---------------- reference curves ---------------------//
    // define the linear curve for eachTwo data
    this.dataEveryTwoPaisVascoLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumPaisVasco < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumPaisVasco < 10000) {
          return yLinearPaisVasco(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return yLinearPaisVasco(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return yLinearPaisVasco(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachTwo data
    this.dataEveryTwoPaisVascoLog = d3
      .line()
      .defined(function(d) {
        if (largestNumPaisVasco < 10000) {
          return d.growthTwo <= 12000;
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return d.growthTwo <= 35000;
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return d.growthTwo <= 75000;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumPaisVasco < 10000) {
          return yLogPaisVasco(
            parseInt(d.growthTwo < 10000 ? d.growthTwo : 10000)
          );
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return yLogPaisVasco(
            parseInt(d.growthTwo < 30000 ? d.growthTwo : 30000)
          );
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return yLogPaisVasco(
            parseInt(d.growthTwo < 60000 ? d.growthTwo : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachThree data
    this.dataEveryThreePaisVascoLinear = d3
      .line()
      .defined(function(d) {
        if (largestNumPaisVasco < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumPaisVasco < 10000) {
          return yLinearPaisVasco(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return yLinearPaisVasco(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return yLinearPaisVasco(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachThree data
    this.dataEveryThreePaisVascoLog = d3
      .line()
      .defined(function(d) {
        if (largestNumPaisVasco < 10000) {
          return d.growthThree <= 10500;
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return d.growthThree <= 34400;
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return d.growthThree <= 68200;
        }
      })
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        if (largestNumPaisVasco < 10000) {
          return yLogPaisVasco(
            parseInt(d.growthThree < 10000 ? d.growthThree : 10000)
          );
        } else if (largestNumPaisVasco > 10000 && largestNumPaisVasco < 30000) {
          return yLogPaisVasco(
            parseInt(d.growthThree < 30000 ? d.growthThree : 30000)
          );
        } else if (largestNumPaisVasco > 30000 && largestNumPaisVasco < 60000) {
          return yLogPaisVasco(
            parseInt(d.growthThree < 60000 ? d.growthThree : 60000)
          );
        }
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFour data
    this.dataEveryFourPaisVascoLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearPaisVasco(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFour data
    this.dataEveryFourPaisVascoLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogPaisVasco(parseInt(d.growthFour));
      })
      .curve(d3.curveBundle.beta(0));

    // define the linear curve for eachFivedata
    this.dataEveryFivePaisVascoLinear = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLinearPaisVasco(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(1));

    // define the logarithmic curve for eachFive data
    this.dataEveryFivePaisVascoLog = d3
      .line()
      .x(function(d) {
        return xFecha(d.fecha);
      })
      .y(function(d) {
        return yLogPaisVasco(parseInt(d.growthFive));
      })
      .curve(d3.curveBundle.beta(0));
    //// --------- end of lines definition ---------- ////

    //// --------- caling the axis --------- ////
    this.yAxisCallLinearPaisVasco = d3.axisLeft(yLinearPaisVasco).ticks(4);
    // this.yAxisCallLinearPaisVasco = d3.axisLeft(yLinearPaisVasco).ticks(4);
    this.yAxisCallLogPaisVasco = d3
      .axisLeft(yLogPaisVasco)
      // .ticks(4)
      .tickValues(
        largestNumPaisVasco < 10000
          ? [1, 10, 100, 1000, 10000]
          : [1, 10, 100, 1000, 10000, 100000]
      )
      .tickArguments([0, ",.0f"]);

    // Add the X Axis
    chartPaisVasco
      .append("g")
      .attr("class", "x-axis-linechart")
      .attr("transform", `translate(0,${height + 10})`)
      .call(d3.axisBottom(xFecha));

    // Add the Y Axis
    chartPaisVasco
      .append("g")
      .attr("class", "y-axis-linechart")
      .call(yAxisCallLinearPaisVasco);

    // Create horizontal grid
    chartPaisVasco
      .selectAll(".y-axis-linechart")
      .selectAll(".tick")
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");

    // -------------- Create curves ---------------- //

    // double each two days curve
    chartPaisVasco
      .append("path")
      .data([timelinePaisVasco])
      .attr("class", "path-line-two")
      .style("stroke", "#19e99e")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryTwoPaisVascoLinear);

    // double each three days curve
    chartPaisVasco
      .append("path")
      .data([timelinePaisVasco])
      .attr("class", "path-line-three")
      .style("stroke", "#ffaa25")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryThreePaisVascoLinear);

    // double each four days curve
    chartPaisVasco
      .append("path")
      .data([timelinePaisVasco])
      .attr("class", "path-line-four")
      .style("stroke", "#a950cf")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFourPaisVascoLinear);

    // double each five days curve
    chartPaisVasco
      .append("path")
      .data([timelinePaisVasco])
      .attr("class", "path-line-five")
      .style("stroke", "#82afff")
      .style("stroke-width", widther > 500 ? "1px" : "1px")
      .style("fill", "none")
      .attr("d", dataEveryFivePaisVascoLinear);

    //// --------- add curve for PaisVasco cases ---------- ////
    chartPaisVasco
      .append("path")
      .data([timelinePaisVasco])
      .attr("class", "path-line")
      .style("stroke", "#fa0067")
      .style("fill", "none")
      .attr("d", valuelineLinearPaisVasco);

    //// --------- end of y line adding ---------- ////

    //// --------- add circles ---------- ////
    chartPaisVasco
      .append("g")
      .attr("class", "path-line-circle")
      .selectAll("circle")
      .data(timelinePaisVasco)
      .enter()
      .append("circle")
      .attr("r", widther > 500 ? 3 : 2)
      .attr("cx", function(d) {
        return xFecha(d.fecha);
      })
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearPaisVasco(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      })
      .style("fill", "#fa0067");

    chartPaisVasco.selectAll("circle").style("display", function(d) {
      return d.regiones[0].data.casosConfirmados === -1 ? "none" : null;
    });
    //// --------- end of adding circles ---------- ////

    //// --------- add legend ---------- ////

    const legendData = [
      { color: "#19e99e", legendName: "2 días" },
      { color: "#ffaa25", legendName: "3 días" },
      { color: "#a950cf", legendName: "4 días" },
      { color: "#82afff", legendName: "5 días" }
    ];

    const legend = chartPaisVasco
      .append("g")
      .attr("class", "line-chart-legend");

    legend
      .selectAll("g")
      .data(legendData)
      .enter()
      .append("g")
      .attr("class", "legend-item")
      .append("rect")
      .attr("class", "legend-rect")
      .attr("y", width - 305)
      .attr("x", function(d, i) {
        return (i + 1.3) * 65;
      })
      .attr("width", 18)
      .attr("height", 2)
      .style("fill", function(d) {
        return d.color;
      });

    // legend.attr("transform", "translate(58,-160)");

    legend
      .append("g")
      .attr("class", "legend-title")
      .append("text")
      .text("Velocidad de duplicación")
      .attr("transform", `translate(-60, ${width - 300})`);

    legend
      .selectAll(".legend-item")
      .append("text")
      .attr("y", width - 300)
      .attr("x", function(d, i) {
        return (i + 1.65) * 65;
      })
      .text(function(d) {
        return d.legendName;
      });

    //// --------- end of adding legend---------- ////
  })
  .catch(function(error) {
    console.log(error);
  });

//// --------- update charts function ---------- ////
function chartUpdatePaisVasco(dataName) {
  if (dataName === "logarithmic") {
    chartPaisVasco
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFivePaisVascoLog);

    chartPaisVasco
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourPaisVascoLog);

    chartPaisVasco
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreePaisVascoLog);

    chartPaisVasco
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoPaisVascoLog);

    chartPaisVasco
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLogPaisVasco);

    chartPaisVasco
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLogPaisVasco(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });

    chartPaisVasco
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLogPaisVasco)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
  if (dataName === "linear") {
    chartPaisVasco
      .selectAll(".path-line-five")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFivePaisVascoLinear);

    chartPaisVasco
      .selectAll(".path-line-four")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryFourPaisVascoLinear);

    chartPaisVasco
      .selectAll(".path-line-three")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryThreePaisVascoLinear);

    chartPaisVasco
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoPaisVascoLinear);

    chartPaisVasco
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLinearPaisVasco);

    chartPaisVasco
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function(d) {
        if (d.regiones[0].data.casosConfirmados != -1) {
          return yLinearPaisVasco(
            d.regiones[0].data.casosConfirmados != 0
              ? d.regiones[0].data.casosConfirmados
              : 1
          );
        } else {
          return null;
        }
      });
    chartPaisVasco
      .selectAll(".y-axis-linechart")
      .transition()
      .duration(600)
      .call(yAxisCallLinearPaisVasco)
      .selectAll("line")
      .attr("x2", widther > 500 ? 420 : 275)
      .attr("stroke", "#eaeaec");
  }
}

//// --------- end of update function ---------- ////
