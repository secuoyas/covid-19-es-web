// append the svg obgect to the body of the page
const chartAragon = d3
  .select("#line-chart-container-2")
  .append("svg")
  .attr("class", "Aragon")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

Promise.all([d3.json("data/spain/covid-19-ES-CCAA-DatosCasos.json")])
  .then((file) => {
    const dataForAragon = file[0];

    // format the data
    dataForAragon.forEach(function (d) {
      d.fecha = parseTime(d.fecha);
    });

    this.dataAragon = [];
    dataForAragon.filter(function (d) {
      if (d.nombre_ccaa === "Aragón") {
        dataAragon.push(d);
      }
    });
    console.log("Aragón:", dataAragon);

    // Add label
    chartAragon
      .append("text")
      .attr("class", "comunity-name")
      .text(dataAragon[0].nombre_ccaa);

    //---------- create array for draw a curve with grow double every two days -------------//
    const logArrAragon = [];

    const getArr = (potencia, periodo, arr) => {
      dataAragon.map((item, idx) => {
        arr.push({ growth: Math.pow(potencia, idx / periodo) });
      });
    };
    getArr(2, 2, logArrAragon);
    console.log("logArrAragon:", logArrAragon);

    const dataEveryTwoAragon = logArrAragon.reverse();

    //// --------- add dataEveryTwo array to rioja array ---------- ////
    dataAragon.map((d, i) => {
      Object.assign(d, dataEveryTwoAragon[i]);
    });

    //// --------- set the ranges ---------- ////
    const x = d3
      .scaleTime()
      .range([0, width])
      .domain(
        d3.extent(dataAragon, function (d) {
          return d.fecha;
        })
      );

    this.yLinearAragon = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        d3.max(dataAragon, function (d) {
          return Math.max(parseInt(d.casosConfirmados));
        }),
      ]);

    this.yLogAragon = d3
      .scaleSymlog()
      .range([height, 0])
      .domain([
        0,
        d3.max(dataAragon, function (d) {
          //   console.log(d.casosConfirmados);
          return Math.max(parseInt(d.casosConfirmados));
        }),
      ]);

    this.yEveryTwoLinearAragon = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        d3.max(dataAragon, function (d) {
          return Math.max(parseInt(d.growth));
        }),
      ]);

    this.yEveryTwoLogAragon = d3
      .scaleSymlog()
      .range([height, 0])
      .domain([
        0,
        d3.max(dataAragon, function (d) {
          return Math.max(parseInt(d.growth));
        }),
      ]);

    //// --------- end the ranges set ---------- ////

    //// --------- define the line ---------- ////
    // define the  linear curve
    this.valuelineLinearAragon = d3
      .line()
      .x(function (d) {
        return x(d.fecha);
      })
      .y(function (d) {
        return yLinearAragon(parseInt(d.casosConfirmados));
      });

    // define the  logarithmic curve
    this.valuelineLogAragon = d3
      .line()
      .x(function (d) {
        return x(d.fecha);
      })
      .y(function (d) {
        return yLogAragon(parseInt(d.casosConfirmados));
      });

    // define the linear curve for eachTwo data
    this.dataEveryTwoAragonLinear = d3
      .line()
      .x(function (d) {
        return x(d.fecha);
      })
      .y(function (d) {
        return yEveryTwoLinearAragon(parseInt(d.growth));
      });

    // define the logarithmic curve for eachTwo data
    this.dataEveryTwoAragonLog = d3
      .line()
      .x(function (d) {
        return x(d.fecha);
      })
      .y(function (d) {
        return yEveryTwoLogAragon(parseInt(d.growth));
      });

    //// --------- end of lines definition ---------- ////

    //// --------- add y gridlines ---------- ////
    chartAragon
      .append("g")
      .attr("class", "line-chart-grid")
      .call(
        d3.axisLeft(yLinearAragon).tickSize(-width).ticks(4).tickFormat("")
      );
    //// --------- end of y gridlines settings ---------- ////

    //// --------- add path to the graphic ---------- ////
    chartAragon
      .append("path")
      .data([dataAragon])
      .attr("class", "path-line")
      .style("stroke", "#fa0067")
      .style("fill", "none")
      .attr("d", valuelineLinearAragon);

    // double each two growths
    chartAragon
      .append("path")
      .data([dataAragon])
      .attr("class", "path-line-two")
      .style("stroke", "#19e99e")
      .style("stroke-width", "3px")
      .style("fill", "none")
      .attr("d", dataEveryTwoAragonLinear);

    //// --------- end of y line adding ---------- ////

    //// --------- add circles ---------- ////
    chartAragon
      .selectAll("circle")
      .data(dataAragon)
      .enter()
      .append("circle")
      .attr("r", 4)
      .attr("cx", function (d) {
        return x(d.fecha);
      })
      .attr("cy", function (d) {
        return yLinearAragon(d.casosConfirmados);
      })
      .style("fill", "#fa0067");

    //// --------- end of adding circles ---------- ////

    //// --------- caling the axis --------- ////
    this.yAxisCallLinearAragon = d3.axisLeft(yLinearAragon).ticks(4);
    this.yAxisCallLogAragon = d3.axisLeft(yLogAragon).ticks(4);

    // Add the X Axis
    chartAragon
      .append("g")
      .attr("class", "x-axis")
      .attr("transform", `translate(0,${height + 10})`)
      .call(d3.axisBottom(x).ticks(4));

    // Add the Y Axis
    chartAragon.append("g").attr("class", "y-axis").call(yAxisCallLinearAragon);

    //// --------- end of caling the axis ---------- ////
  })
  .catch(function (error) {
    console.log(error);
  });

//// --------- update charts ---------- ////
function chartUpdateAragon(dataName) {
  if (dataName === "logarithmic") {
    chartAragon
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoAragonLog);

    chartAragon
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLogAragon);

    chartAragon
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function (d) {
        return yLogAragon(d.casosConfirmados);
      });
    chartAragon
      .selectAll(".y-axis")
      .transition()
      .duration(600)
      .call(yAxisCallLogAragon);

    // update the Y gridlines
    chartAragon
      .selectAll(".line-chart-grid")
      .transition()
      .duration(600)
      .call(d3.axisLeft(yLogAragon).tickSize(-width).ticks(4).tickFormat(""));
  }
  if (dataName === "linear") {
    chartAragon
      .selectAll(".path-line-two")
      .transition()
      .duration(600)
      .attr("d", this.dataEveryTwoAragonLinear);
    chartAragon
      .selectAll(".path-line")
      .transition()
      .duration(600)
      .attr("d", this.valuelineLinearAragon);

    chartAragon
      .selectAll("circle")
      .transition()
      .duration(600)
      .attr("cy", function (d) {
        return yLinearAragon(parseInt(d.casosConfirmados));
      });
    chartAragon
      .selectAll(".y-axis")
      .transition()
      .duration(600)
      .call(yAxisCallLinearAragon);

    // update the Y gridlines
    chartAragon
      .selectAll(".line-chart-grid")
      .transition()
      .duration(600)
      .call(
        d3.axisLeft(yLinearAragon).tickSize(-width).ticks(4).tickFormat("")
      );
  }
}

//// --------- end of update cahrts function ---------- ////
