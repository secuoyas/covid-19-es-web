"use strict";
// ---------- menu button logic ----------//
const menuButtonClose = document.querySelector(".nav-menu-btn-close");
const menuButtonOpen = document.querySelector(".nav-menu-btn-open");
const menu = document.querySelector(".nav-container");

menuButtonClose.addEventListener("click", handleClose);
menuButtonOpen.addEventListener("click", handleOpen);

function handleOpen() {
  menu.classList.add("show");
  document.body.classList.add("menu-open");
}
function handleClose() {
  menu.classList.remove("show");
  document.body.classList.remove("menu-open");
}

// ---------- menu background logic ----------//
const menuItemArr = Array.from(document.querySelectorAll(".nav-item"));
const navBg = document.querySelector(".nav-bg");

menuItemArr.forEach((el) => {
  el.addEventListener("mouseover", function (event) {
    const imageName = event.currentTarget.getAttribute("data-image");
    navBg.style.backgroundImage = `url('img/${imageName}')`;
  });

  el.addEventListener("mouseout", function (event) {
    const navBgAtr = navBg.getAttribute("data-image-default");
    navBg.style.backgroundImage = `url('img/${navBgAtr}')`;
  });
});


