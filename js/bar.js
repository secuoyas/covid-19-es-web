"use strict";

Promise.all([
  d3.json("data/spain/populatonCCAA.json"),
  d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/?ultimodia=true"),
])
  .then((data) => {
    const dataForBar = data[1].timeline[0].regiones.map((item) => {
      return item.data;
    });

    // create arrays of datas with last update
    const casosConfirmados = [];
    const casosFallecidos = [];
    const population = [];
    dataForBar.slice(0, 19).map((item) => {
      casosConfirmados.push(parseInt(item.casosConfirmados));
      casosFallecidos.push(parseInt(item.casosFallecidos));
    });

    data[0].map((item) => {
      population.push(item.population);
    });

    // Getting total population
    const totalPopulation = population.reduce(function (a, b) {
      return a + b;
    }, 0);

    // Getting sum of confirmed cases
    const sumConfirmed = casosConfirmados.reduce(function (a, b) {
      return a + b;
    }, 0);

    // Getting sum of cases of death
    const sumDeath = casosFallecidos.reduce(function (a, b) {
      return a + b;
    }, 0);

    const conutPercentage = (base, num) => {
      return Math.ceil((parseInt(num) / parseInt(base)) * 10000) / 100 + "%";
    };

    // Insert a numers and percentage to the page
    const totalConfirmed = document.querySelector("#total-confirmed");
    const totalDeath = document.querySelector("#total-death");
    const percentageConfirmed = document.querySelector("#percentage-confirmed");
    const percentageDeath = document.querySelector("#percentage-death");
    const barConfirmed = document.querySelector("#bar-inner-confirmed");
    const barDeath = document.querySelector("#bar-inner-death");

    totalConfirmed.innerHTML = sumConfirmed
      .toString()
      .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
    totalDeath.innerHTML = sumDeath
      .toString()
      .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
    percentageConfirmed.innerHTML = conutPercentage(
      totalPopulation,
      sumConfirmed
    );
    percentageDeath.innerHTML = conutPercentage(sumConfirmed, sumDeath);

    barConfirmed.style.width = conutPercentage(totalPopulation, sumConfirmed);
    barDeath.style.width = conutPercentage(sumConfirmed, sumDeath);
  })
  .catch(function (error) {
    console.log(error);
  });
