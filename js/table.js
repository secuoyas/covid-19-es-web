"use strict";
Promise.all([d3.json("https://covid19.secuoyas.io/api/v1/es/ccaa/")])
  .then((data) => {
    const fechaArr = [];
    const regionesArr = [];

    data[0].timeline.map((item) => {
      fechaArr.push(item.fecha);
      regionesArr.push(item.regiones);
    });

    const allObjArr = [];

    regionesArr.map((item) => {
      item.map((subItem) => {
        allObjArr.push(subItem);
      });
    });

    //------------- get comunities names for first column -----------------//

    const communityNames = data[0].timeline[0].regiones.map((item) => {
      return item.nombreLugar;
    });

    //------------- Andalucía -----------------//
    let andaluciaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Andalucía") {
        andaluciaArr.push(d);
      }
    });

    andaluciaArr.map((d, i) => {
      Object.assign(d, { ["fecha"]: fechaArr[i] });
    });

    andaluciaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- Aragón -----------------//
    let aragonArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Aragón") {
        aragonArr.push(d);
      }
    });

    aragonArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Asturias" -----------------//
    let asturiasArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Asturias") {
        asturiasArr.push(d);
      }
    });

    asturiasArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Baleares" -----------------//
    let balearesArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Baleares") {
        balearesArr.push(d);
      }
    });

    balearesArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Canarias" -----------------//
    let canariasArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Canarias") {
        canariasArr.push(d);
      }
    });

    canariasArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Cantabria" -----------------//
    let cantabriaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Cantabria") {
        cantabriaArr.push(d);
      }
    });

    cantabriaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Castilla-La Mancha" -----------------//
    let castillaLaManchaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Castilla-La Mancha") {
        castillaLaManchaArr.push(d);
      }
    });

    castillaLaManchaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Castilla y León" -----------------//
    let castillaYLeonArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Castilla y León") {
        castillaYLeonArr.push(d);
      }
    });

    castillaYLeonArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Cataluña" -----------------//
    let catalunaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Cataluña") {
        catalunaArr.push(d);
      }
    });

    catalunaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Ceuta" -----------------//
    let ceutaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Ceuta") {
        ceutaArr.push(d);
      }
    });

    ceutaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Comunidad Valenciana" -----------------//
    let comunidadValencianaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Comunidad Valenciana") {
        comunidadValencianaArr.push(d);
      }
    });

    comunidadValencianaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Extremadura" -----------------//
    let extremaduraArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Extremadura") {
        extremaduraArr.push(d);
      }
    });

    extremaduraArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Galicia" -----------------//
    let galiciaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Galicia") {
        galiciaArr.push(d);
      }
    });

    galiciaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Madrid" -----------------//
    let madridArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Madrid") {
        madridArr.push(d);
      }
    });

    madridArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Melilla" -----------------//
    let melillaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Melilla") {
        melillaArr.push(d);
      }
    });

    melillaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Murcia" -----------------//
    let murciaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Murcia") {
        murciaArr.push(d);
      }
    });

    murciaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "Navarra" -----------------//
    let navarraArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Navarra") {
        navarraArr.push(d);
      }
    });

    navarraArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "País Vasco" -----------------//
    let paisVascoArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "Pais Vasco") {
        paisVascoArr.push(d);
      }
    });

    paisVascoArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- "La Rioja" -----------------//
    let laRiojaArr = [];
    allObjArr.filter(function (d) {
      if (d.nombreLugar === "La Rioja") {
        laRiojaArr.push(d);
      }
    });

    laRiojaArr.forEach(function (d) {
      d.fecha = d3.timeParse("%Y-%m-%d")(d.fecha);
    });

    //------------- Create table -----------------//
    function tabulate() {
      // name table

      const firstColumn = d3
        .select("#table")
        .append("div")
        .attr("class", "table-names")
        .append("table");
      const firstColumnThead = firstColumn.append("thead");
      const firstColumnTbody = firstColumn
        .append("tbody")
        .attr("class", "table-names-tbody");

      firstColumnThead
        .append("tr")
        .append("th")
        .attr("class", "table-name-column-th")
        .text("Comunidad Autónoma");

      firstColumnTbody
        .selectAll("tr")
        .data(communityNames)
        .enter()
        .append("tr")
        .append("td")
        .attr("class", "name-column-td")
        .text(function (d) {
          return d;
        });

      // content table
      const table = d3
        .select("#table")
        .append("div")
        .attr("class", "table-content")
        .append("table");

      const thead = table.append("thead");
      const tbody = table.append("tbody");

      // append the header row
      thead.append("tr").attr("class", "top-line");

      // Count how many days and month in array
      let monthArr = [];
      andaluciaArr.map((item, idx) => {
        monthArr.push(item.fecha.toDateString().slice(4, 7));
      });

      // append month name in to the header row
      thead
        .selectAll(".top-line")
        .data(andaluciaArr)
        .append("th")
        .attr("class", "month")
        .attr("colspan", function () {
          return monthArr.filter((x) => x === "Feb").length;
        })
        .text("Feb");

      // append month name in to the header row
      thead
        .selectAll(".top-line")
        .data(andaluciaArr)
        .append("th")
        .attr("class", "month")
        .attr("colspan", function () {
          return monthArr.filter((x) => x === "Mar").length;
        })
        .text("Mar");

      // append month name in to the header row
      thead
        .selectAll(".top-line")
        .data(andaluciaArr)
        .append("th")
        .attr("class", "month")
        .attr("colspan", function () {
          return monthArr.filter((x) => x === "Apr").length;
        })
        .text("Apr");

      // append month name in to the header row
      thead
        .selectAll(".top-line")
        .data(andaluciaArr)
        .append("th")
        .attr(
          "class",
          monthArr.filter((x) => x === "May").length ? "month" : "cell-empty"
        )
        .attr("colspan", function () {
          return monthArr.filter((x) => x === "May").length;
        })
        .text(monthArr.filter((x) => x === "May").length ? "May" : null);

      // append each monday in to the header row
      thead
        .append("tr")
        .attr("class", "second-line")
        .selectAll("fecha")
        .data(andaluciaArr)
        .enter()
        .append("th")
        .attr("class", "fecha")
        .text(function (d, i) {
          return widther > 610
            ? d.fecha.toDateString().slice(8, 10)
            : widther < 610 && d.fecha.toDateString().slice(8, 10) % 4 === 0
            ? d.fecha.toDateString().slice(8, 10)
            : null;
        });

      //------------- Andalucía -----------------//
      const rowsAndalucia = tbody
        .append("tr")
        .attr("class", "andalucia")
        .selectAll("td")
        .data(andaluciaArr)
        .enter()
        .append("td")

        .attr("class", function (d) {
          if (
            parseInt(d.data.casosConfirmados) == 0 ||
            parseInt(d.data.casosConfirmados) == null
          ) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- Aragón -----------------//
      const rowsAragon = tbody
        .append("tr")
        .attr("class", "aragon")
        .selectAll("td")
        .data(aragonArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- Asturias -----------------//
      const rowsAsturias = tbody
        .append("tr")
        .attr("class", "asturias")
        .selectAll("td")
        .data(asturiasArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Baleares" -----------------//
      const rowsBaleares = tbody
        .append("tr")
        .attr("class", "baleares")
        .selectAll("td")
        .data(balearesArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Canarias" -----------------//
      const rowsCanarias = tbody
        .append("tr")
        .attr("class", "canarias")
        .selectAll("td")
        .data(canariasArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Cantabria" -----------------//
      const rowsCantabria = tbody
        .append("tr")
        .attr("class", "cantabria")
        .selectAll("td")
        .data(cantabriaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Castilla-La Mancha"" -----------------//
      const rowsCastillaLaMancha = tbody
        .append("tr")
        .attr("class", "castilla-la-mancha")
        .selectAll("td")
        .data(castillaLaManchaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Castilla y León" -----------------//
      const rowsCastillaYLeonArr = tbody
        .append("tr")
        .attr("class", "castilla-y-leon")
        .selectAll("td")
        .data(castillaYLeonArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Cataluña" -----------------//
      const rowsCataluna = tbody
        .append("tr")
        .attr("class", "cataluna")
        .selectAll("td")
        .data(catalunaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Ceuta" -----------------//
      const rowsCeuta = tbody
        .append("tr")
        .attr("class", "ceuta")
        .selectAll("td")
        .data(ceutaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Comunidad Valenciana" -----------------//
      const rowsComunidadValenciana = tbody
        .append("tr")
        .attr("class", "comunidad-valenciana")
        .selectAll("td")
        .data(comunidadValencianaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Extremadura" -----------------//
      const rowsExtremadura = tbody
        .append("tr")
        .attr("class", "extremadura")
        .selectAll("td")
        .data(extremaduraArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Galicia" -----------------//
      const rowsGalicia = tbody
        .append("tr")
        .attr("class", "galicia")
        .selectAll("td")
        .data(galiciaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Madrid" -----------------//
      const rowsMadrid = tbody
        .append("tr")
        .attr("class", "madrid")
        .selectAll("td")
        .data(madridArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Melilla" -----------------//
      const rowsMelilla = tbody
        .append("tr")
        .attr("class", "melilla")
        .selectAll("td")
        .data(melillaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Murcia" -----------------//
      const rowsMurcia = tbody
        .append("tr")
        .attr("class", "murcia")
        .selectAll("td")
        .data(murciaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "Navarra" -----------------//
      const rowsNavarra = tbody
        .append("tr")
        .attr("class", "navarra")
        .selectAll("td")
        .data(navarraArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "País Vasco" -----------------//
      const rowsPaisVasco = tbody
        .append("tr")
        .attr("class", "pais-vasco")
        .selectAll("td")
        .data(paisVascoArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });

      //------------- "La Rioja" -----------------//
      const rowsLaRioja = tbody
        .append("tr")
        .attr("class", "rioja")
        .selectAll("td")
        .data(laRiojaArr)
        .enter()
        .append("td")
        // .text(function(d) {
        //   return d.data.casosConfirmados;
        // })
        .attr("class", function (d) {
          if (parseInt(d.data.casosConfirmados) == 0) {
            return "empty";
          } else if (parseInt(d.data.casosConfirmados) < 10) {
            return "light";
          } else if (
            parseInt(d.data.casosConfirmados) > 10 &&
            parseInt(d.data.casosConfirmados) < 99
          ) {
            return "middle";
          } else if (
            parseInt(d.data.casosConfirmados) > 99 &&
            parseInt(d.data.casosConfirmados) < 999
          ) {
            return "saturated";
          } else if (parseInt(d.data.casosConfirmados) > 999) {
            return "oversaturated";
          }
        });
    }

    tabulate();
  })
  .catch(function (error) {
    console.log(error);
  });
